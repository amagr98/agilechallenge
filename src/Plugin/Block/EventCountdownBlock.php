<?php
namespace Drupal\event_countdown\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Throwable;

/**
 * Provides a 'Event Countdown' Block.
 *
 * @Block(
 *   id = "event_countdown",
 *   admin_label = @Translation("Event countdown"),
 *   category = @Translation("Custom block"),
 * )
 */
class EventCountdownBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    try{
      return [
        '#markup' => $this->getDaysLeft(),
        '#cache' => [
          'max-age' => 0
        ]
      ];
    }
    catch (Throwable $e)
    {
      //just some basic try catching, so whole page doesn't fail if there is error in this module.

      //also should log error somewhere so you can fix it
      return null;
    }
  }

  /**
   * Private function for getting days left to event
   */
  private function getDaysLeft() {

    $service = \Drupal::service('event_countdown.custom_services');

    $node = \Drupal::routeMatch()->getParameter('node');
    $nid = $node->id();
    $nodeEntity = \Drupal\node\Entity\Node::load($nid);
    $eventDateString = $nodeEntity->get('field_event_date')->getValue()[0]['value'];

    $eventDate =  new \DateTime($eventDateString);
    
    return $service->getDaysLeft($eventDate);
  }

}
