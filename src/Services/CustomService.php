<?php

namespace Drupal\event_countdown\Services;

/**
 * Class CustomService.
 * we have few options how to structure this class. for example:
 *    1: just have all of methods indepentednt from each other - pass data to methods, instead of populating class attributes in constructor
 *    2: put whole logic for date of event in this class, and put date in private varaible, and set in constructor (for example,
 *    we would have methods for event countdown, for getting average temperature of previous year for that day, weather forecast for that day,...) 
 *    3: have whole logic for these node type in one class, and load whole node data in variable(s) in constructor

 *    I think option 1 is most suitable for that feature.
 * 
 */
class CustomService{

  public function getDaysLeft(\DateTime $eventDate){

    $dateToday = new \DateTime('today');

    $dayDiff = $dateToday->diff($eventDate)->format("%r%a"); //stupid php datetime format... Carbon is much nicer :)
    $dayDiff = (int) $dayDiff;

    if($dayDiff > 0)
    {
      $dayString = $dayDiff > 1 ? "are $dayDiff days" : "is $dayDiff day";
      /*simple logic for just english language, which has very simple rule. If I had other languages, 
      like slovenian, I would create helper function that I would use on whole project to determine which string to use. */
      return "Until event there $dayString left!";
    }

    if($dayDiff < 0)
    {
      return "Event already happened!";
    }

    return "Event is happening today!";
  }
}